from hashlib import md5
from pathlib import Path
from pickle import dump, HIGHEST_PROTOCOL
from textwrap import indent

import pandas as pd

from libworkflows import run_and_write_methods
from libsnp import load_counts_from_joint_vcf, counts2freqs

freebayes_version = "v1.3.3"
module_versions = {
    "fastqc": "0.11.9",
    "freebayes": "v1.3.3",
    "snpEff": "4.3",
    "cutadapt": "2.10",
    "bwa": "0.7.17",
    "graalvm": "ce-java19-22.3.1",
    "R": "3.6.2",
    "picard-tools": "2.23.3",
    "samtools": "1.18",
    "java": "1.8.0",
    "bamtools": "2.5.2",
    # http://qualimap.conesalab.org/doc_html/analysis.html#multi-sample-bam-qc
    # http://qualimap.conesalab.org/doc_html/command_line.html#cmdline-multibamqc
    "qualimap": "v2.2.1",
    "tabix": "0.2.6",
    "GenomeAnalysisTK": "4.1.9.0",
    # A USER ERROR has occurred: RealignerTargetCreator is no longer included in GATK as of version 4.0.0.0. Please use GATK3 to run this tool
    # A USER ERROR has occurred: IndelRealigner is no longer included in GATK as of version 4.0.0.0. Please use GATK3 to run this tool
    #"GenomeAnalysisTK": "3.6",
    "htslib": "1.10",
    "vcflib": "1.0.1"
}


longpart = config.get("longpart", "long")
outdir = Path(config.get("outdir", "."))
outdir.mkdir(exist_ok=True, parents=True)
methods_dir = outdir.joinpath("methods")
methods_dir.mkdir(exist_ok=True, parents=True)
genome_dir = Path(config["genome_dir"])
chr_sizes = {}
with genome_dir.joinpath("genome.chrSize").open() as fh:
    for line in fh:
        if line[0] == "#":
            continue
        [chr_name, chr_size] = line.split()
        chr_sizes[chr_name] = int(chr_size)

chromosomes = list(chr_sizes.keys())

class Chromosome():
    def __init__(self, name, length, part_len):
        self.name = name
        self.length = length
        self.parts_dict = self.set_parts(part_len)
        self.parts = [part_num for part_num in self.parts_dict.keys()]

    def set_parts(self, part_len):
        parts_dict = {}
        part_num = 1
        start = 0
        end = part_len
        while end < self.length:
            parts_dict[str(part_num)] = (start, end)
            start = end
            end = end + part_len
            part_num += 1
        parts_dict[str(part_num)] = (start, self.length)
        return parts_dict

part_len = 50000

chrom_dict = {
    chrom_name: Chromosome(chrom_name, chrom_size, part_len)
    for (chrom_name, chrom_size) in chr_sizes.items()}

sample_dirs = config["sample_dirs"]
samples = tuple(sorted(sample_dirs.keys()))

m = md5()
for sample in samples:
    m.update(sample.encode())

md5hash = m.hexdigest()
md52samples = {md5hash: samples}
samples2md5 = {samples: md5hash}

by_chrom = True
by_part = True

if by_part:
    chrom_parts = [
        outdir.joinpath("chromosome_parts.bed")]

final_vcfs = [
    outdir.joinpath("FB", md5hash, "FB.vcf.gz"),
    outdir.joinpath("FB", md5hash, "FB.vcf.gz.tbi")]

annotated_vcfs = []
for final_vcf in final_vcfs:
    if final_vcf.suffix == ".gz":
        assert Path(final_vcf.stem).suffix == ".vcf"
        annotated_vcf = final_vcf.parent.joinpath(
            f"{Path(final_vcf.stem).stem}.snpEFF.vcf.gz")
        annotated_vcf_index = final_vcf.parent.joinpath(
            f"{Path(final_vcf.stem).stem}.snpEFF.vcf.gz.tbi")
        annotated_vcfs.extend([annotated_vcf, annotated_vcf_index])

snp_tables = []
for annotated_vcf in annotated_vcfs:
    if annotated_vcf.suffix == ".tbi":
        continue
    snp_table = annotated_vcf.parent.joinpath(
        f"{Path(Path(annotated_vcf.stem).stem).stem}.snpEFF.tsv")
    snp_tables.append(snp_table)

wildcard_constraints:
    md5hash="\w+"


def make_resource_setter(*res_values):
    """
    Make a function that sets a resource according to an attempt number.

    `res_values` should be the values to use for attempts 1, 2, 3, etc.
    in that order.
    If the attempt number is higher than the number of values provided,
    the last one will be used.

    The returned function will take wildcards, attempt and threads as
    arguments, in that order, so that it can be used in the resources
    section of a snakemake rule.
    """
    attempt2resource = dict(enumerate(res_values, start=1))
    def set_resource(wildcards, attempt, threads):
        return attempt2resource.get(attempt, res_values[-1])
    return set_resource


#def sample2bam(wildcards):
#    return Path(sample_dirs[wildcards.sample]).joinpath(f"{wildcards.sample}.bam")


def source_bams(wildcards):
    return [
        Path(sample_dirs[sample]).joinpath(f"{sample}.bam")
        for sample in md52samples[wildcards.md5hash]]


rule all:
    input:
        chrom_parts,
        #outdir.joinpath("FB", md5hash, "bam_list.txt"),
        final_vcfs,
        annotated_vcfs,
        snp_tables,
        #[
        #    outdir.joinpath("GATK", f"{sample}_HC.g.vcf.gz")
        #    for sample in samples],
        #outdir.joinpath("md5hashes.pickle"),


rule store_chrom_parts:
    output:
        bed = outdir.joinpath("chromosome_parts.bed")
    run:
        with open(output.bed, "w") as fh:
            for chrom_name in chromosomes:
                chromosome = chrom_dict[chrom_name]
                for part in chromosome.parts:
                    (start, end) = chromosome.parts_dict[part]
                    fh.write(f"{chrom_name}\t{start}\t{end}\t{chrom_name}_{part}\n")

rule make_bam_list:
    input:
        bams=source_bams,
    output:
        outdir.joinpath("FB", "{md5hash}", "bam_list.txt")
    run:
        with open(output[0], "wt") as fh:
            fh.write("\n".join(input.bams))


rule store_hashdicts:
    output:
        hashfile=outdir.joinpath("md5hashes.pickle")
    run:
        with open(hashfile, "wb") as fh:
            dump(md52samples, fh, HIGHEST_PROTOCOL)
            dump(samples2md5, fh, HIGHEST_PROTOCOL)
            

rule make_chr_list:
    output:
        chrlist=outdir.joinpath("chromosomes.list")
    run:
        chrfile = genome_dir.joinpath("genome.chrSize")
        shell(f"cut -f 1 {chrfile} > {output.chrlist}")


# What about this freebayes option?
"""
   -A --cnv-map FILE
        Read a copy number map from the BED file FILE, which has
        either a sample-level ploidy:
            sample_name copy_number
        or a region-specific format:
            seq_name start end sample_name copy_number
        ... for each region in each sample which does not have the
        default copy number as set by --ploidy. These fields can be delimited
        by space or tab.

"""


#def get_mem_mb(wildcards, attempt):
#    return (100 ** attempt) // 2

if not by_part:
    rule jointcall_FB_by_chrom:
        input:
            bams=source_bams,
            bam_list=outdir.joinpath("FB", "{md5hash}", "bam_list.txt"),
        output:
            vcf=outdir.joinpath("FB", "{md5hash}", "{chrom}", "FB_{chrom}.vcf.gz"),
            vcf_idx=outdir.joinpath("FB", "{md5hash}", "{chrom}", "FB_{chrom}.vcf.gz.tbi"),
            methods=methods_dir.joinpath("FB", "{md5hash}", "{chrom}", "freebayes_{chrom}_methods.md"),
        params:
            ref_fasta=genome_dir.joinpath("genome.fa"),
            modules = ["freebayes", "tabix"]
        resources:
            mem_mb=10240
            #mem_mb=get_mem_mb
        run:
            #input_part = " ".join([
            #    f"--bam {bam}"
            #    for bam in input.bams])
            input_part = f"-L {input.bam_list}"
            chrom_end = chr_sizes[wildcards.chrom]
            region_part = f"--region {wildcards.chrom}:0-{chrom_end}"
            other_opts="--pooled-continuous --limit-coverage 200"
            cmd = f"freebayes {other_opts} -f {params.ref_fasta} {input_part} {region_part} | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
            run_and_write_methods(cmd, params, output, module_versions)
else:
    methods = f"""
SNP calling was performed jointly on the bam files for all the included samples
using freebayes {freebayes_version} with the help of a Snakemake workflow
in order to parallellize the process across chromosomes, and across
{part_len} bp portions within each chromosome.
Option `--pooled-continuous` of freebayes was used.
"""
    rule jointcall_FB_by_chrom_by_part:
        input:
            bams=source_bams,
            bam_list=outdir.joinpath("FB", "{md5hash}", "bam_list.txt"),
        output:
            vcf=temp(outdir.joinpath("FB", "{md5hash}", "{chrom}", "FB_{chrom}_{part}.vcf.gz")),
            vcf_idx=temp(outdir.joinpath("FB", "{md5hash}", "{chrom}", "FB_{chrom}_{part}.vcf.gz.tbi")),
            methods=methods_dir.joinpath("FB", "{md5hash}", "{chrom}", "freebayes_{chrom}_{part}_methods.md"),
        params:
            ref_fasta=genome_dir.joinpath("genome.fa"),
            modules = ["freebayes", "tabix"]
        resources:
            # QOS long is only available on partition long, and can be used for jobs with runtime up to 365d
            # QOS normal can be used for jobs with runtime up to 1d (1440m)
            # QOS fast can be used for jobs with runtime up to 2h (120m).
            # QOS ultrafast can be used for jobs with runtime up to 5m.
            mem_mb=make_resource_setter(4096, 10240, 16384, 32768),
            runtime=make_resource_setter(120, 60 * 24, 60 * 24 * 60, 60 * 24 * 365),
            slurm_extra=make_resource_setter("'--qos=fast'", "'--qos=normal'", f"'--qos=long' '--partition={longpart}'", f"'--qos=long' '--partition={longpart}'"),
        #    runtime=21900,
        #    slurm_extra="'--qos=long'"
        run:
            #input_part = " ".join([
            #    f"--bam {bam}"
            #    for bam in input.bams])
            input_part = f"-L {input.bam_list}"
            (part_start, part_end) = chrom_dict[wildcards.chrom].parts_dict[wildcards.part]
            region_part = f"--region {wildcards.chrom}:{part_start}-{part_end}"
            other_opts="--pooled-continuous --limit-coverage 200"
            cmd = f"freebayes {other_opts} -f {params.ref_fasta} {input_part} {region_part} | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
            run_and_write_methods(cmd, params, output, module_versions, methods_before=methods)

    def source_vcf_chrom_parts(wildcards):
        return [
            outdir.joinpath("FB", f"{wildcards.md5hash}", f"{wildcards.chrom}", f"FB_{wildcards.chrom}_{part}.vcf.gz")
            for part in chrom_dict[wildcards.chrom].parts]

    def source_vcf_idx_chrom_parts(wildcards):
        return [
            outdir.joinpath("FB", f"{wildcards.md5hash}", f"{wildcards.chrom}", f"FB_{wildcards.chrom}_{part}.vcf.gz.tbi")
            for part in chrom_dict[wildcards.chrom].parts]

    rule merge_vcf_parts_for_chrom:
        input:
            vcfs_in = source_vcf_chrom_parts,
            vcfs_idx_in = source_vcf_idx_chrom_parts,
        output:
            vcf=temp(outdir.joinpath("FB", "{md5hash}", "{chrom}", "FB_{chrom}.vcf.gz")),
            vcf_idx=temp(outdir.joinpath("FB", "{md5hash}", "{chrom}", "FB_{chrom}.vcf.gz.tbi")),
            methods=methods_dir.joinpath("FB", "{md5hash}", "{chrom}", "merge_vcf_methods.md"),
        params:
            #modules = ["samtools"]
            modules = ["htslib", "vcflib", "tabix"]
        resources:
            runtime=make_resource_setter(1, 5),
        run:
            #cmd = f"bcftools concat --naive -o {output.vcf} -O z --write-index {input}"
            cmd=f"zcat {input.vcfs_in} | vcffirstheader | vcfstreamsort -w 1000 | vcfuniq | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
            run_and_write_methods(cmd, params, output, module_versions)


rule merge_vcf:
    input:
        vcfs_in = expand(outdir.joinpath("FB", "{{md5hash}}", "{chrom}", "FB_{chrom}.vcf.gz"), chrom=chromosomes),
        vcfs_idx_in = expand(outdir.joinpath("FB", "{{md5hash}}", "{chrom}", "FB_{chrom}.vcf.gz.tbi"), chrom=chromosomes),
    output:
        vcf=outdir.joinpath("FB", "{md5hash}", "FB.vcf.gz"),
        vcf_idx=outdir.joinpath("FB", "{md5hash}", "FB.vcf.gz.tbi"),
        methods=methods_dir.joinpath("FB", "{md5hash}", "merge_vcf_methods.md"),
    params:
        #modules = ["samtools"]
        modules = ["htslib", "vcflib", "tabix"]
    run:
        #cmd = f"bcftools concat --naive -o {output.vcf} -O z --write-index {input}"
        cmd=f"zcat {input.vcfs_in} | vcffirstheader | vcfstreamsort -w 1000 | vcfuniq | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
        run_and_write_methods(cmd, params, output, module_versions)


rule annotate_with_snpEFF:
    input:
        vcf_in="{prefix}.vcf.gz",
    output:
        vcf="{prefix}.snpEFF.vcf.gz",
        vcf_idx="{prefix}.snpEFF.vcf.gz.tbi",
        summary="{prefix}.snpEFF_summary",
        methods="{prefix}.snpEFF_methods.md",
        #methods=methods_dir.joinpath(
        #    Path("{prefix}.snpEFF_methods.md").relative_to(os.path.commonpath([
        #        Path(f"{prefix}.snpEFF_methods.md"),
        #        methods_dir])))
        # methods=outdir.joinpath("methods", "{prefix}.snpEFF_methods.md"),
    params:
        modules=["graalvm", "snpEff", "htslib"],
        config=genome_dir.joinpath("snpEff", "snpEff.config")
    run:
        cmd = f"snpEff -ud 0 genome {input.vcf_in} -noLog -c {params.config} -stats {output.summary} | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}"
        run_and_write_methods(cmd, params, output, module_versions)


rule extract_snp_table:
    input:
        vcf_in="{prefix}.snpEFF.vcf.gz",
    output:
        tsv="{prefix}.snpEFF.tsv",
        #methods=outdir.joinpath("{prefix}.extract_snp_table_methods.md")
    params:
    run:
        all_counts = load_counts_from_joint_vcf(
            input.vcf_in,
            chromosomes,
            biallelic_only=False,
            snp_only=True,
            allow_mnp=False)
        all_freqs = counts2freqs(all_counts)
        pd.concat([all_counts, all_freqs], axis=1).to_csv(output.tsv, sep="\t")
