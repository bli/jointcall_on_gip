#!/bin/bash

# Usage: QOS=<slurm qos> PART=<splurm partition> ./run_workflow.sh [<snakemake_config> [<other snakemake options>]]

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#   ----------------------------------------------------------------
#   Function for exit due to fatal program error
#       Accepts 1 argument:
#           string containing descriptive error message
#   ----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error in ${WORKFLOW}"}" 1>&2
    exit 1
}

function print_eval
{
    cmd="$@"
    >&2 echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
}


if [[ ${VENV} ]]
then
    source ${VENV}/bin/activate
fi

[ ${QOS} ] || QOS="hubbioit"
[ ${PART} ] || PART="hubbioit"

SCRIPT_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
WORKFLOW="jointcall"
profile="${SCRIPT_DIR}/profile/slurm"
[ -d ${profile} ] || error_exit "Missing profile directory: ${profile}"

[ ${PROFILE} ] || PROFILE="${SCRIPT_DIR}/workflow/profile"
workflow_profile="${PROFILE}"
[ -d ${workflow_profile} ] || error_exit "Missing workflow profile directory: ${workflow_profile}"

# TODO: Problem if we want to have options to pass to snakemake but no config file
if [ "${1}" ]
then
    snakemake_config=${1}
    config_option="--configfile ${snakemake_config}"
    shift
else
    config_option=""
fi

snakefile="${SCRIPT_DIR}/workflow/${WORKFLOW}.snakefile"
[ -e ${snakefile} ] || error_exit "Missing snakefile: ${snakefile}"
echo "Snakemake version: $(snakemake --version)"

cmd="mkdir -p logs/cluster/${WORKFLOW}"
print_eval ${cmd}

cmd="snakemake -s ${snakefile} ${config_option} \
    --executor slurm
    --profile ${profile} \
    --workflow-profile ${workflow_profile} \
    $@"

echo "${cmd}"

# --mem=10G to avoid a bug
# See https://github.com/snakemake/snakemake/issues/2230#issuecomment-1521386382
>&2 sbatch --qos="${QOS}" --partition="${PART}" --parsable \
    -J run_${WORKFLOW} \
    --mem=10G \
    -o logs/cluster/${WORKFLOW}/${WORKFLOW}.o \
    -e logs/cluster/${WORKFLOW}/${WORKFLOW}.e \
    ${cmd} \
    || error_exit "sbatch snakemake failed for ${WORKFLOW}"

exit 0
